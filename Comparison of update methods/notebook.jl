### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# ╔═╡ 455f6d24-2c51-11eb-3ac6-8f4a12258c55
begin
	using HeteroscedasticPCA
	using HeteroscedasticPCA: HPPCA
	using HeteroscedasticPCA: ExpectationMaximization, DifferenceOfConcave,
		RootFinding, QuadraticSolvableMinorizer, CubicSolvableMinorizer
	using HeteroscedasticPCA: loglikelihood
	using HeteroscedasticPCA: updatev!, updateF!
	
	using BenchmarkTools, Dates, IdentityRanges, LaTeXStrings, LinearAlgebra, Manifolds, OrderedCollections, Plots, Plots.Measures, PlutoUI, Random
	ENV["MPLBACKEND"]="Agg";  # fix from: https://github.com/fonsp/Pluto.jl/issues/511#issuecomment-711430965
	pyplot(markerstrokewidth=0.5,size=(250,225),dpi=300,guidefontsize=11,tickfontsize=10,
		legendfontsize=6.5,fg_color_legend=nothing,bg_color_legend=nothing,
		markerstrokecolor=:auto)
	
	using CacheVariables
	Core.eval(Main, :(using Dates, HeteroscedasticPCA, OrderedCollections))
	CACHEDIR = "cache"
	OUTPUTDIR = "outs"
	
	HeteroscedasticPCA.updatev!(M::HPPCA,v::Vector{<:Real}) = (M.v .= v; M)
	Base.isequal(M1::HPPCA,M2::HPPCA) = all(getfield(M1,f) == getfield(M2,f) for f in fieldnames(HPPCA))
	vlineup!(f,x,ymin=ylims()[1];vargs...) = plot!(fill(x,2),[ymin,f(x)];marker=:circle,vargs...)
	
	html"""<style>
	main {
		max-width: 100%;
		margin-right: 0;
	}
	"""
end

# ╔═╡ 346d06d4-2c51-11eb-2e03-55a12d26ec38
md"""
# Comparison of Update Methods
"""

# ╔═╡ 6c416b2e-3aa8-11ed-2952-799bd0701b29
md"""
## Utilities
"""

# ╔═╡ 41dccf30-3812-11eb-1fb9-01c2b8d38310
function genmodel(d,λ,v)
	k, L = length(λ), length(v)
	U = rand(uniform_distribution(Stiefel(d,k),zeros(d,k)))
	return HPPCA(U,λ,I(k),v)
end
#;

# ╔═╡ 112a2cf2-2d2c-11eb-3133-193b5ab49ccd
function gendata(M::HPPCA,n)
	d, k = size(M.U)
	L = length(M.v)
	F = M.U*sqrt(Diagonal(M.λ))
	Y = [F*randn(k,n[l]) + sqrt(M.v[l])*randn(d,n[l]) for l in 1:L]
	return Y
end
#;

# ╔═╡ fcd44410-3823-11eb-0400-6996a42113e1
function factordiff(M1,M2)
	cov1 = M1.U*Diagonal(M1.λ)*M1.U'
	cov2 = M2.U*Diagonal(M2.λ)*M2.U'
	return norm(cov1-cov2)/norm(cov1)
end
#;

# ╔═╡ 21bc8af8-3810-11eb-07bc-851f2cc08517
function homppca(Y,k)
	L, n = length(Y), size.(Y,2)
	cor = sum(Y[l]*Y[l]' for l in 1:L)/sum(n)
	λh, Uh = eigen(Hermitian(cor),sortby=-)
	λb = mean(λh[k+1:end])
	HPPCA(Uh[:,1:k],λh[1:k] .- λb,I(k),fill(λb,L))
end
#;

# ╔═╡ e7bea1f6-3812-11eb-3cdb-91c95e445b44
function heppcat(Y,k,T)
	M = homppca(Y,k)
	for _ in 1:T
		updatev!(M,Y,ExpectationMaximization())
		updateF!(M,Y,ExpectationMaximization())
	end
	M
end
#;

# ╔═╡ 04505e24-3822-11eb-1730-a921b368a3c6
md"""
## Methods
"""

# ╔═╡ 99170496-3aa9-11ed-2b10-93228d7a6deb
md"""
### Global maximization
"""

# ╔═╡ 5555ae44-381f-11eb-0070-61a42d29a48c
function v_global(M,Y,l)
	U, λ, vtl, Yl = M.U, M.λ, M.v[l], Y[l]
	d, k = size(U)
	nl = size(Yl,2)
	
	α = [j == 0 ? d-k : 1 for j in IdentityRange(0:k)]
	β = [j == 0 ? norm((I-U*U')*Yl)^2/nl : norm(Yl'*U[:,j])^2/nl for j in IdentityRange(0:k)]
	γ = [j == 0 ? zero(eltype(λ)) : λ[j] for j in IdentityRange(0:k)]
	
	LL = vl -> -sum(α[j]*log(γ[j]+vl)+β[j]/(γ[j]+vl) for j in 0:k)
	
	return vl -> LL(vl)-LL(vtl)
end
#;

# ╔═╡ b8b83acc-3aa9-11ed-013c-c1d0db047889
md"""
### Expectation Maximization
"""

# ╔═╡ 0ca604ae-3822-11eb-3289-a5121e43344c
function v_em(M,Y,l)
	U, Λ, vtl, Yl = M.U, Diagonal(M.λ), M.v[l], Y[l]
	d = size(U,1)
	nl = size(Yl,2)
	
	FMFt = U*Λ*inv(Λ+vtl*I)*U'
	rho = norm((I-FMFt)*Yl)^2/nl + vtl*tr(FMFt)
	minorizer = vl -> -d*log(vl) - rho/vl
	
	return vl -> minorizer(vl)-minorizer(vtl)
end
#;

# ╔═╡ c3986872-3aa9-11ed-3560-3fc003421616
md"""
### Difference of concave
"""

# ╔═╡ 2ed36a60-3822-11eb-3a9d-3d07bbc7b0ec
function v_doc(M,Y,l)
	U, λ, vtl, Yl = M.U, M.λ, M.v[l], Y[l]
	d, k = size(U)
	nl = size(Yl,2)
	
	α = [j == 0 ? d-k : 1 for j in IdentityRange(0:k)]
	β = [j == 0 ? norm((I-U*U')*Yl)^2/nl : norm(Yl'*U[:,j])^2/nl for j in IdentityRange(0:k)]
	γ = [j == 0 ? zero(eltype(λ)) : λ[j] for j in IdentityRange(0:k)]
	
	minorizer = vl -> -sum(α[j]/(γ[j]+vtl)*vl + β[j]/(γ[j]+vl) for j in 0:k)
	
	return vl -> minorizer(vl)-minorizer(vtl)
end
#;

# ╔═╡ 2ec89e8a-3aaa-11ed-3b8d-576d7dd74018
md"""
### Quadratic solvable minorizer
"""

# ╔═╡ 4fbbc628-3822-11eb-175a-397dca661c38
function v_quad(M,Y,l)
	U, λ, vtl, Yl = M.U, M.λ, M.v[l], Y[l]
	d, k = size(U)
	nl = size(Yl,2)
	
	α = [j == 0 ? d-k : 1 for j in IdentityRange(0:k)]
	β = [j == 0 ? norm((I-U*U')*Yl)^2/nl : norm(Yl'*U[:,j])^2/nl for j in IdentityRange(0:k)]
	γ = [j == 0 ? zero(eltype(λ)) : λ[j] for j in IdentityRange(0:k)]
	
	J0 = findall(iszero,γ)
	αtl = sum(α[j] for j in J0)
	βtl = sum(β[j] for j in J0)
	ζtl = sum(α[j]/(γ[j]+vtl) for j in 0:k if j ∉ J0)
	B = βtl + sum(β[j]*vtl^2/(γ[j]+vtl)^2 for j in 0:k if j ∉ J0)
	minorizer = vl -> -αtl*log(vl) - B/vl - ζtl*vl
	
	return vl -> minorizer(vl)-minorizer(vtl)
end
#;

# ╔═╡ 3a50f0ba-3aaa-11ed-0f07-fd68dc912ff8
md"""
### Cubic solvable minorizer
"""

# ╔═╡ 78a98598-3822-11eb-0b05-2ba54b48de79
function v_cubic(M,Y,l)
	U, λ, vtl, Yl = M.U, M.λ, M.v[l], Y[l]
	d, k = size(U)
	nl = size(Yl,2)
	
	α = [j == 0 ? d-k : 1 for j in IdentityRange(0:k)]
	β = [j == 0 ? norm((I-U*U')*Yl)^2/nl : norm(Yl'*U[:,j])^2/nl for j in IdentityRange(0:k)]
	γ = [j == 0 ? zero(eltype(λ)) : λ[j] for j in IdentityRange(0:k)]
	
	c = [-2*β[j]/γ[j]^3 for j in IdentityRange(0:k)]
	J0 = findall(iszero,γ)
	αtl = sum(α[j] for j in J0)
	βtl = sum(β[j] for j in J0)
	ζtl = sum(α[j]/(γ[j]+vtl) for j in 0:k if j ∉ J0)
	γtl = -ζtl + sum(β[j]/(γ[j]+vtl)^2 for j in 0:k if j ∉ J0)
	ctl = sum(c[j] for j in 0:k if j ∉ J0)
	minorizer = vl -> -αtl*log(vl) - βtl/vl + γtl*vl + 1/2*ctl*(vl-vtl)^2
	
	return vl -> minorizer(vl)-minorizer(vtl)
end
#;

# ╔═╡ c9783f78-3aaa-11ed-1d54-5f0ea340576b
md"""
### Labels / colors
"""

# ╔═╡ fc855024-38c5-11eb-3972-7baf69d16359
vminorizers = OrderedDict(
	"Difference of concave"        => (v_doc,   DifferenceOfConcave(),        palette(:default)[1]),
	"Cubic solvable minorizer"     => (v_cubic, CubicSolvableMinorizer(),     palette(:default)[2]),
	"Expectation Maximization"     => (v_em,    ExpectationMaximization(),    palette(:default)[3]),
	"Quadratic solvable minorizer" => (v_quad,  QuadraticSolvableMinorizer(), palette(:default)[4]),
);

# ╔═╡ ea5e1b3a-3c38-11eb-0e25-a16520252e48
methodlist = OrderedDict("Global maximization"=>(v=RootFinding(),F=ExpectationMaximization(),lc=RGB(0,0,0)),
	[name=>(v=vmethod,F=ExpectationMaximization(),lc=linecolor) for (name,(_,vmethod,linecolor)) in vminorizers]...);

# ╔═╡ 7cb795ac-380f-11eb-28d8-87d087e587b8
md"""
## Setup
"""

# ╔═╡ 3cdf1996-2d30-11eb-2f94-075ed7e6b037
d = 100;

# ╔═╡ 9cabbeaa-2c53-11eb-07b5-c5533b00907b
n = [200,800];

# ╔═╡ 4c0331b8-2c53-11eb-24ef-a7f06f94004a
v1 = 1.0;

# ╔═╡ 9e20a138-2c53-11eb-1d16-abbfacddc40d
λ = [4.0,2.0,1.0];

# ╔═╡ 281e47d4-2d31-11eb-1e02-6bd383f23a0d
k = length(λ);

# ╔═╡ 372eecb2-2d31-11eb-1ed3-b9ca3162b9ec
L = length(n);

# ╔═╡ 5d17475e-3813-11eb-05d1-2bba64ef37d3
maxiters = 1000;

# ╔═╡ 9afb9296-2c52-11eb-1795-67d0c5b0a198
md"""
## Figure 2
"""

# ╔═╡ 2f00cfaa-3813-11eb-136d-739ffc811019
M1 = @cache joinpath(CACHEDIR,"sim1,M.bson") (Random.seed!(0); genmodel(d,λ,[v1,1.0]));

# ╔═╡ 3a4d6f8a-3813-11eb-342e-bbc44b35a863
Y1 = @cache joinpath(CACHEDIR,"sim1,Y.bson") (Random.seed!(0); gendata(M1,n));

# ╔═╡ c9c0da90-3c3b-11eb-3609-2977ad695239
traces1 = @cache joinpath(CACHEDIR,"sim1,traces.bson") let niters=30
	Y = deepcopy(Y1)
	
	(OrderedDict∘map)(collect(methodlist)) do (name,(vmethod,Fmethod,_))
		# iterates
		M = homppca(Y,k)
		iterates = [deepcopy(M)]
		for _ in 1:niters
			updatev!(M,Y,vmethod); updateF!(M,Y,Fmethod)
			push!(iterates,deepcopy(M))
		end
		
		# timings
		alltimings = map(1:100) do _
			M = homppca(Y,k)
			walltimes = [0.0]
			gcstate = GC.enable(false)
			for t in 1:niters
				etime = @elapsed (updatev!(M,Y,vmethod); updateF!(M,Y,Fmethod))
				M == iterates[t+1] || throw("Iterates do not match!")
				push!(walltimes,walltimes[end]+etime)
			end
			GC.enable(gcstate)
			return walltimes
		end
		GC.gc()
		medwalltimes = [median(getindex.(alltimings,t)) for t in 1:niters+1]
		meanwalltimes = [mean(getindex.(alltimings,t)) for t in 1:niters+1]

		name=>(iterates=iterates,medwalltimes=medwalltimes,meanwalltimes=meanwalltimes,timestamp=now())
	end
end;

# ╔═╡ 79a11c12-3aab-11ed-2cd4-f7d1183b8aec
md"""
### Figure 2a
"""

# ╔═╡ 4bc0d8da-3c41-11eb-39ef-51d97210013c
let irange=1:1:25
	fullrange = first(irange):last(irange)
	plot(xlabel=L"\textrm{median walltime (ms)}",ylabel=L"\mathcal{L}^\star - \mathcal{L}(\mathbf{F}_t,\mathbf{v}_t)")
	
	Lopt = loglikelihood(heppcat(Y1,k,maxiters),Y1)
	for ((name,(models,medtimes,meantimes,_)),lw,ms) in zip(traces1,[3;fill(2,4)],[5;fill(4,4)])
		plot!(1000*medtimes[fullrange],Lopt.-loglikelihood.(models[fullrange],Ref(Y1)),
			color=methodlist[name].lc,linewidth=lw,label=name)
		scatter!(1000*medtimes[irange],Lopt.-loglikelihood.(models[irange],Ref(Y1)),
			color=methodlist[name].lc,marker=:circle,markersize=ms,label="")
	end
	plot!(xlims=(0,10),ylims=(1e-10,2e-1),yscale=:log10)
	plot!(legend=(0.15,0.58))

	savefig(joinpath(OUTPUTDIR,"figure-2a.png")); plot!(dpi=100)
	plot!(legend=:outertop,size=(250,225+5*15))
end

# ╔═╡ 8147efb8-3aab-11ed-1aa7-93030faa3443
md"""
### Figure 2b
"""

# ╔═╡ 5b235238-3c41-11eb-1bd4-539ee70fb52d
let irange=1:1:25
	fullrange = first(irange):last(irange)
	plot(xlabel=L"\textrm{median walltime (ms)}",
		ylabel=L"\|\mathbf{F}_t\mathbf{F}_t'-{\mathbf{F}^\star}{\mathbf{F}^\star}'\|_{\mathrm{F}}/\|{\mathbf{F}^\star}{\mathbf{F}^\star}'\|_{\mathrm{F}}",
		left_margin=5mm,top_margin=8mm)
	
	Mopt = heppcat(Y1,k,maxiters)
	for ((name,(models,medtimes,meantimes,_)),lw,ms) in zip(traces1,[3;fill(2,4)],[5;fill(4,4)])
		plot!(1000*medtimes[fullrange],factordiff.(Ref(Mopt),models[fullrange]),
			color=methodlist[name].lc,linewidth=lw,label=name)
		scatter!(1000*medtimes[irange],factordiff.(Ref(Mopt),models[irange]),
			color=methodlist[name].lc,marker=:circle,markersize=ms,label="")
	end
	plot!(xlims=(0,10),ylims=(3e-7,2e-3),yscale=:log10)
	plot!(legend=nothing)
	
	savefig(joinpath(OUTPUTDIR,"figure-2b.png")); plot!(dpi=100)
	plot!(legend=:outertop,size=(250,225+5*15))
end

# ╔═╡ 74a39c08-3aab-11ed-2c1b-95279cf18a01
md"""
### Figure 2c
"""

# ╔═╡ ff901cd4-38c2-11eb-0174-9d235c90cabd
map(1:L) do l
	M, Y = homppca(Y1,k), Y1
	vtl, vrange = M.v[l], 0.1:0.01:4.25
	
	plot(xlabel=latexstring("v_$l"),ylabel=latexstring("\\mathcal{L}_$l(v_$l)-\\mathcal{L}_$l(v_{t,$l})"))
	plot!(v_global(M,Y,l),vrange,color=:black,label="Global maximization",linewidth=3)
	vlineup!(v_global(M,Y,l),updatev!(deepcopy(M),Y,RootFinding()).v[l],label="",color=:black,linewidth=3,markersize=5)
	
	for (name,(func,method,linecolor)) in vminorizers
		plot!(func(M,Y,l),vrange,label=name,color=linecolor,linestyle=:dash,linewidth=2)
		vlineup!(func(M,Y,l),updatev!(deepcopy(M),Y,method).v[l],label="",color=linecolor,linewidth=2,markersize=4)
	end
	
	plot!(xlims=(0.75,1.25),ylims=(-5,1),legend=nothing)
	savefig(joinpath(OUTPUTDIR,"figure-2c-$l.png")); plot!(dpi=100)
end |> plts -> let
	plot(plts...,layout=(1,2),size=(2*250,225))
	plot!(legend=:outertop,size=(2*250,225+5*15))
end

# ╔═╡ abe6889c-38e1-11eb-0923-fdc089e824eb
md"""
## Figure 3
"""

# ╔═╡ ac2b20c4-38e1-11eb-334d-853a6d4db8b8
M2 = @cache joinpath(CACHEDIR,"sim2,M.bson") (Random.seed!(0); genmodel(d,λ,[v1,4.0]));

# ╔═╡ 8dac538c-38f4-11eb-215b-8ba24beca375
Y2 = @cache joinpath(CACHEDIR,"sim2,Y.bson") (Random.seed!(0); gendata(M2,n));

# ╔═╡ 1778f452-3c3c-11eb-0495-c149b6eb58e5
traces2 = @cache joinpath(CACHEDIR,"sim2,traces.bson") let niters=150
	Y = deepcopy(Y2)
	
	(OrderedDict∘map)(collect(methodlist)) do (name,(vmethod,Fmethod,_))
		# iterates
		M = homppca(Y,k)
		iterates = [deepcopy(M)]
		for _ in 1:niters
			updatev!(M,Y,vmethod); updateF!(M,Y,Fmethod)
			push!(iterates,deepcopy(M))
		end
		
		# timings
		alltimings = map(1:100) do _
			M = homppca(Y,k)
			walltimes = [0.0]
			gcstate = GC.enable(false)
			for t in 1:niters
				etime = @elapsed (updatev!(M,Y,vmethod); updateF!(M,Y,Fmethod))
				M == iterates[t+1] || throw("Iterates do not match!")
				push!(walltimes,walltimes[end]+etime)
			end
			GC.enable(gcstate)
			return walltimes
		end
		GC.gc()
		medwalltimes = [median(getindex.(alltimings,t)) for t in 1:niters+1]
		meanwalltimes = [mean(getindex.(alltimings,t)) for t in 1:niters+1]

		name=>(iterates=iterates,medwalltimes=medwalltimes,meanwalltimes=meanwalltimes,timestamp=now())
	end
end;

# ╔═╡ c234abb0-3aab-11ed-3601-fd6b0bb38691
md"""
### Figure 3a
"""

# ╔═╡ 5500ca9c-3c3d-11eb-0765-9bf6d50db013
let irange=1:5:126
	fullrange = first(irange):last(irange)
	plot(xlabel=L"\textrm{median walltime (ms)}",ylabel=L"\mathcal{L}^\star - \mathcal{L}(\mathbf{F}_t,\mathbf{v}_t)")
	
	Lopt = loglikelihood(heppcat(Y2,k,maxiters),Y2)
	for ((name,(models,medtimes,meantimes,_)),lw,ms) in zip(traces2,[3;fill(2,4)],[5;fill(4,4)])
		plot!(1000*medtimes[fullrange],Lopt.-loglikelihood.(models[fullrange],Ref(Y2)),
			color=methodlist[name].lc,linewidth=lw,label=name)
		scatter!(1000*medtimes[irange],Lopt.-loglikelihood.(models[irange],Ref(Y2)),
			color=methodlist[name].lc,marker=:circle,markersize=ms,label="")
	end
	plot!(xlims=(0,33),ylims=(1e-8,3e4),yticks=(10.0).^(-8:2:4),yscale=:log10)
	plot!(legend=(-0.05,-0.05))

	savefig(joinpath(OUTPUTDIR,"figure-3a.png")); plot!(dpi=100)
	plot!(legend=:outertop,size=(250,225+5*15))
end

# ╔═╡ d21d88bc-3aab-11ed-3656-f91cf8a27f40
md"""
### Figure 3b
"""

# ╔═╡ 84d0804a-3c3e-11eb-00c2-77b7bcc7ed05
let irange=1:5:126
	fullrange = first(irange):last(irange)
	plot(xlabel=L"\textrm{median walltime (ms)}",
		ylabel=L"\|\mathbf{F}_t\mathbf{F}_t'-{\mathbf{F}^\star}{\mathbf{F}^\star}'\|_{\mathrm{F}}/\|{\mathbf{F}^\star}{\mathbf{F}^\star}'\|_{\mathrm{F}}",
		left_margin=5mm,top_margin=8mm)
	
	Mopt = heppcat(Y2,k,maxiters)
	for ((name,(models,medtimes,meantimes,_)),lw,ms) in zip(traces2,[3;fill(2,4)],[5;fill(4,4)])
		plot!(1000*medtimes[fullrange],factordiff.(Ref(Mopt),models[fullrange]),
			color=methodlist[name].lc,linewidth=lw,label=name)
		scatter!(1000*medtimes[irange],factordiff.(Ref(Mopt),models[irange]),
			color=methodlist[name].lc,marker=:circle,markersize=ms,label="")
	end
	plot!(xlims=(0,33),ylims=(1e-5,2e0),yscale=:log10)
	plot!(legend=nothing)
	
	savefig(joinpath(OUTPUTDIR,"figure-3b.png")); plot!(dpi=100)
	plot!(legend=:outertop,size=(250,225+5*15))
end

# ╔═╡ ab14e3e6-3aab-11ed-3d0e-df1fdc26b37c
md"""
### Figure 3c
"""

# ╔═╡ 9780da84-38f4-11eb-2c1c-b5f8d588f75e
map(1:L) do l
	M, Y = homppca(Y2,k), Y2
	vtl, vrange = M.v[l], 0.1:0.01:4.25
	
	plot(xlabel=latexstring("v_$l"),ylabel=latexstring("\\mathcal{L}_$l(v_$l)-\\mathcal{L}_$l(v_{t,$l})"))
	plot!(v_global(M,Y,l),vrange,color=:black,label="Global maximization",linewidth=3)
	vlineup!(v_global(M,Y,l),updatev!(deepcopy(M),Y,RootFinding()).v[l],label="",color=:black,linewidth=3,markersize=5)
	
	for (name,(func,method,linecolor)) in vminorizers
		plot!(func(M,Y,l),vrange,label=name,color=linecolor,linestyle=:dash,linewidth=2)
		vlineup!(func(M,Y,l),updatev!(deepcopy(M),Y,method).v[l],label="",color=linecolor,linewidth=2,markersize=4)
	end
	scatter!([vtl],[0.0],color=:black,markersize=5,label="")
	
	if l == 1
		plot!(xlims=(0.75,3.75),ylims=(-20,60),legend=nothing)
		lens!([1.00,1.07],[46.95,49.60],inset=(1,bbox(0.57,0.04,0.4,0.4)),subplot=2,ticks=nothing,framestyle=:box)
	elseif l == 2
		plot!(xlims=(2.98,4.0),ylims=(-0.5,1.5),legend=nothing)
		lens!([3.945,3.97],[1.312,1.362],inset=(1,bbox(0.05,0.04,0.4,0.4)),subplot=2,ticks=nothing,framestyle=:box)
	end
	
	savefig(joinpath(OUTPUTDIR,"figure-3c-$l.png")); plot!(dpi=100)
end |> plts -> let
	plot(plts...,layout=(1,2),size=(2*250,225))
	plot!(legend=:outertop,size=(2*250,225+5*15))
end

# ╔═╡ Cell order:
# ╟─346d06d4-2c51-11eb-2e03-55a12d26ec38
# ╠═455f6d24-2c51-11eb-3ac6-8f4a12258c55
# ╟─6c416b2e-3aa8-11ed-2952-799bd0701b29
# ╠═41dccf30-3812-11eb-1fb9-01c2b8d38310
# ╠═112a2cf2-2d2c-11eb-3133-193b5ab49ccd
# ╠═fcd44410-3823-11eb-0400-6996a42113e1
# ╠═21bc8af8-3810-11eb-07bc-851f2cc08517
# ╠═e7bea1f6-3812-11eb-3cdb-91c95e445b44
# ╟─04505e24-3822-11eb-1730-a921b368a3c6
# ╟─99170496-3aa9-11ed-2b10-93228d7a6deb
# ╠═5555ae44-381f-11eb-0070-61a42d29a48c
# ╟─b8b83acc-3aa9-11ed-013c-c1d0db047889
# ╠═0ca604ae-3822-11eb-3289-a5121e43344c
# ╟─c3986872-3aa9-11ed-3560-3fc003421616
# ╠═2ed36a60-3822-11eb-3a9d-3d07bbc7b0ec
# ╟─2ec89e8a-3aaa-11ed-3b8d-576d7dd74018
# ╠═4fbbc628-3822-11eb-175a-397dca661c38
# ╟─3a50f0ba-3aaa-11ed-0f07-fd68dc912ff8
# ╠═78a98598-3822-11eb-0b05-2ba54b48de79
# ╟─c9783f78-3aaa-11ed-1d54-5f0ea340576b
# ╠═fc855024-38c5-11eb-3972-7baf69d16359
# ╠═ea5e1b3a-3c38-11eb-0e25-a16520252e48
# ╟─7cb795ac-380f-11eb-28d8-87d087e587b8
# ╠═3cdf1996-2d30-11eb-2f94-075ed7e6b037
# ╠═9cabbeaa-2c53-11eb-07b5-c5533b00907b
# ╠═4c0331b8-2c53-11eb-24ef-a7f06f94004a
# ╠═9e20a138-2c53-11eb-1d16-abbfacddc40d
# ╠═281e47d4-2d31-11eb-1e02-6bd383f23a0d
# ╠═372eecb2-2d31-11eb-1ed3-b9ca3162b9ec
# ╠═5d17475e-3813-11eb-05d1-2bba64ef37d3
# ╟─9afb9296-2c52-11eb-1795-67d0c5b0a198
# ╠═2f00cfaa-3813-11eb-136d-739ffc811019
# ╠═3a4d6f8a-3813-11eb-342e-bbc44b35a863
# ╠═c9c0da90-3c3b-11eb-3609-2977ad695239
# ╟─79a11c12-3aab-11ed-2cd4-f7d1183b8aec
# ╠═4bc0d8da-3c41-11eb-39ef-51d97210013c
# ╟─8147efb8-3aab-11ed-1aa7-93030faa3443
# ╠═5b235238-3c41-11eb-1bd4-539ee70fb52d
# ╟─74a39c08-3aab-11ed-2c1b-95279cf18a01
# ╠═ff901cd4-38c2-11eb-0174-9d235c90cabd
# ╟─abe6889c-38e1-11eb-0923-fdc089e824eb
# ╠═ac2b20c4-38e1-11eb-334d-853a6d4db8b8
# ╠═8dac538c-38f4-11eb-215b-8ba24beca375
# ╠═1778f452-3c3c-11eb-0495-c149b6eb58e5
# ╟─c234abb0-3aab-11ed-3601-fd6b0bb38691
# ╠═5500ca9c-3c3d-11eb-0765-9bf6d50db013
# ╟─d21d88bc-3aab-11ed-3656-f91cf8a27f40
# ╠═84d0804a-3c3e-11eb-00c2-77b7bcc7ed05
# ╟─ab14e3e6-3aab-11ed-3d0e-df1fdc26b37c
# ╠═9780da84-38f4-11eb-2c1c-b5f8d588f75e
