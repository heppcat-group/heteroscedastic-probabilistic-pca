# Comparison of update methods

Code to reproduce the comparison of update methods (Figures 2 and 3):
+ `notebook.jl`: main file (a [`Pluto.jl`](https://github.com/fonsp/Pluto.jl) notebook)
+ `outs/`: figures produced by the main file
+ `cache/`: cache of simulation runs
+ `JuliaProject.toml` and `JuliaManifest.toml`: files specifying the Julia packages used (see [https://pkgdocs.julialang.org/v1.5/environments/](https://pkgdocs.julialang.org/v1.5/environments/))
+ `matplotlibrc`: file specifying `matplotlib` settings

## Installing relevant packages

This code has been tested on Julia v1.5.2.
The versions of the Julia packages used are recorded in `Manifest.toml`,
and can be installed by instantiating the environment
in the Julia package manager:
```
julia> import Pkg; Pkg.activate(@__DIR__); Pkg.instantiate()
```
For more info see: [https://pkgdocs.julialang.org/v1.5/environments/#Using-someone-else's-project](https://pkgdocs.julialang.org/v1.5/environments/#Using-someone-else's-project)

### Note
The code uses `matplotlib` internally to make the plots,
but the version of `matplotlib` installed may be newer and no longer compatible.
In particular,
the code has been tested using `matplotlib` at v3.1.1 and `python` at v3.8.13.

If `matplotlib` has been installed automatically by `PyPlot.jl` (via `Conda.jl`),
the older versions may be installed as follows
(note that the Julia packages will likely need to be rebuilt after):
```
julia> import PyPlot
julia> PyPlot.PyCall.Conda.add("python=3.8.13")
julia> PyPlot.PyCall.Conda.add("matplotlib=3.1.1")
julia> import Pkg; Pkg.build()
```
**Warning:**
Note that this uses the root conda environment of `Conda.jl`
so may be affected by (and may affect)
other projects on your computer that use the same environment.
It may be prudent to first export the current environment:
[https://docs.conda.io/projects/conda/en/4.13.x/user-guide/tasks/manage-environments.html#sharing-an-environment](https://docs.conda.io/projects/conda/en/4.13.x/user-guide/tasks/manage-environments.html#sharing-an-environment)
