# HePPCAT: Probabilistic PCA for Data with Heteroscedastic Noise

Repository containing code for reproducing the results in the paper
> David Hong, Kyle Gilman, Laura Balzano, Jeffrey A. Fessler. "HePPCAT: Probabilistic PCA for Data with Heteroscedastic Noise", IEEE Tr. on Signal Processing 69:4819-34, Aug. 2021. http://doi.org/10.1109/TSP.2021.3104979 http://arxiv.org/abs/2101.03468.

**Note:**
The `HePPCAT` method proposed in the paper is available as a registered Julia package: https://github.com/dahong67/HePPCAT.jl

Questions? Reach us at: dahong67@wharton.upenn.edu

## Directories
+ **`Comparison of update methods/`:**
  Code to reproduce the comparison of update methods (Figures 2 and 3).
+ **`Statistical performance/`:**
  Code to reproduce the statistical performance experiments (Figures 4-9 and 14).
